# Teste para React Native Developer

[![Lyncas Logo](https://img-dev.feedback.house/TCo5z9DrSyX0EQoakV8sJkx1mSg=/fit-in/300x300/smart/https://s3.amazonaws.com/feedbackhouse-media-development/modules%2Fcore%2Fcompany%2F5c9e1b01c5f3d0003c5fa53b%2Flogo%2F5c9ec4f869d1cb003cb7996d)](https://www.lyncas.net)
### Requisitos

- React Native (https://facebook.github.io/react-native/)
- React (https://facebook.github.io/react/)
- Material Design (https://material.io/guidelines/)
- Native Components
- JSX

### Diferencial

- Testes unitários
- Arquitetura do projeto

## Como participar?

1. Faça um clone deste repositório.
2. Quando estiver finalizado a prova, compartilhe seu código no seu Git de preferência e nos envie para o e-mail que consta ao final desse documento.
3. Faremos nossa análise e te daremos um retorno.

## Detalhes da prova

- A prova consiste em criar um app simples de Lista de Contatos.
- Seu projeto deve também conter um arquivo README com a explicação das tecnologias utilizadas e as instruções para rodar.
- Descrever suas dificuldades e facilidades, bem como o número de horas de desenvolvimento.

### Critérios analisados

- Arquitetura do projeto (camadas)
- Funcionalidades e funcionamento

### Funcionalidades

O app deve conter as seguintes funcionalidades:

1. Navegação com dados do usuário e itens de menu
2. Listagem de contatos
3. Criação de novo contato
4. Edição e exclusão de contatos
5. Exibir alerta de confirmaçao de exclusão

*Obs.: Página de login é opcional, mas será considerado um diferencial*

### Especificações técnicas

* O app deve se comunicar com uma base de dados externa via REST API.
* A entidade *contato* deve possuir os campos: nome, sobrenome, telefone, endereço (rua, bairro, cidade, uf, número), e-mail

## Dúvidas? Envio da prova?
`testes@lyncas.net`

### Desde já obrigado pelo seu interesse e boa prova!